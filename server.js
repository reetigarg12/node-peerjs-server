// server.js
// where your node app starts

// we've started you off with Express (https://expressjs.com/)
// but feel free to use whatever libraries or frameworks you'd like through `package.json`.
const express = require("express");
const app = express();
const server = require('http').Server(app);
var io = require('socket.io')(server, { origins: '*:*'});
const ExpressPeerServer = require('peer').ExpressPeerServer;
var cors = require('cors')

var options = {
    debug: true
}
app.use(cors());
app.options('*', cors());

// our default array of dreams


app.use(express.static("public"));


// https://expressjs.com/en/starter/basic-routing.htmsl


app.get("/test", (request, response) => {
    console.log("calling test!");
    response.status(200).json({success: true, data: 'Success'});
});
const listener = server.listen(3000, () => {
    console.log("Your app is listening on port " + listener.address().port);
});


io.on('connection', socket => {
    socket.on('join-room', (roomId, userId) => {
        console.log(`Person ${userId} joined room ${roomId}`);
        socket.join(roomId)
        socket.to(roomId).broadcast.emit('user-connected', userId)

        socket.on('disconnect', () => {
            console.log("Socket Disconnected!")
            socket.to(roomId).broadcast.emit('user-disconnected', userId)
        })
    })
});

// send the default array of dreams to the webpage
// listen for requests :)

const peerServer = ExpressPeerServer(server, {
    debug: true
});
app.use('/peerjs', peerServer);
peerServer.on('connection', (client) => { console.log('PeerJS connected');});
peerServer.on('disconnect', (client) => { console.log("PeerJS disconnected")});


